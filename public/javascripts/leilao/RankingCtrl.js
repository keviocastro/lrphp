function RankingCtrl($scope, $http) {
	
	var s = $scope;
	
	 s.itensOrcamento = [];
	 s.ranking = [];
	 s.orcamentoSelecionado = {};
	 s.posicaoSelecionada = 1;
	 s.msgTitulo = 'Aqui está o melhor preço';

	 jq = jQuery;

	 var Init = function() {
	 	s.obterRanking(jq('#leilao_numero').val());

	 };

	 s.obterRanking = function(leilao_numero) {
	 	$http.get('/rest/leilao/ranking/'+leilao_numero).success(function(res) {
	 		if (res.s) {
	 			for (var i = res.r.length - 1; i >= 0; i--) {
	 				if (i == 0) {
	 					res.r[i].clsPosicao = "badge badge-success";
	 					res.r[i].clsTotal = "text-success";
	 					jq("#localEntrega").geocomplete({
						  map: ".mapa",	
						  location: res.r[i].leilao_localentrega
						});
	 				}else{
	 					res.r[i].clsPosicao = "badge";
	 				}
	 				totalizar(res.r[i]);
	 				res.r[i].posicao = i+1;
	 			};
	 			$scope.ranking = res.r;
	 			$scope.obterItensPorOrcamento(res.r[0].orcamento_numero, 1);
	 		}else{
	 			window.alert(res.msg);
	 		}
		   
		});
	 };

	 s.obterItensPorOrcamento = function(orcamento_numero, orcamento_posicao) {
	 	$http.get('/rest/itemorcamento/'+orcamento_numero).success(function(res) {
	    	if (res.s) {
	    		for (var i = res.r.length - 1; i >= 0; i--) {
	    			res.r[i].itemorcamento_valortotal = jQuery.number(res.r[i].itemorcamento_valortotal, 2, ',', '.' );
	    		}
	    		s.itensOrcamento = res.r;
	    		s.posicaoSelecionada = orcamento_posicao;
	    		for (var i = $scope.ranking.length - 1; i >= 0; i--) {
	    			if (s.ranking[i].orcamento_numero == orcamento_numero){
	    				s.ranking[i].clsMenuSel = 'info';
	    				s.orcamentoSelecionado = s.ranking[i];
	    			}else{
	    				s.ranking[i].clsMenuSel = '';
	    			}
	    		}
	    		if (orcamento_posicao == 1){
	    			jQuery('#spanPosicaoSel').attr('class', 'badge badge-success');
	    			jQuery('#titulo').attr('class', 'text-success');
	    			s.msgTitulo = 'Aqui está o melhor preço';
	    		}else{
	    			jQuery('#spanPosicaoSel').attr('class', 'badge');
	    			s.msgTitulo = 'Esse é o '+orcamento_posicao+'º melhor preço';
	    			jQuery('#titulo').attr('class', 'text');
	    		}
	    	}else{
	    		window.alert(res.msg);
	    	}
		});
	 };

	 var totalizar = function(orcamento) {
	 	orcamento.total_com_entrega = parseFloat(orcamento.orcamento_valorentrega) + parseFloat(orcamento.totalorcamento);
		orcamento.total_com_entrega = jQuery.number(orcamento.total_com_entrega, 2, ',', '.' );
		orcamento.orcamento_valorentrega = jQuery.number(orcamento.orcamento_valorentrega, 2, ',', '.' );
	 };

	 Init();
};