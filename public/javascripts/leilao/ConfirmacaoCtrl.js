function ConfirmacaoCtrl($scope, $http) {

	$scope.leilao = {};
	$scope.msgNaoNotificando = 'Marque aqui se você quer ser notificado por e-mail a cada orçamento feito.';
	$scope.msgNotificando = 'Você está sendo notificado, por e-mail, a cada orcamento criado.';

	var Init = function() {
		obterLeilao(jQuery('#leilao_numero').val());
	};

	var checarNotificacao = function() {
		if ($scope.leilao.leilao_acompanhar) {
			$scope.msgNotificar = $scope.msgNotificando;
			$scope.leilao.leilao_acompanhar = true; // Porque o angularjs não entende 1 e 0
		}else{
			$scope.msgNotificar = $scope.msgNaoNotificando;
			$scope.leilao.leilao_acompanhar = false;
		}
	};

	$scope.notificacao = function(){
		jQuery('#checkNotificar').attr("disabled", true);

		$http.put('/rest/leilao/',{
			leilao_numero: jQuery('#leilao_numero').val(),
			leilao_acompanhar: $scope.leilao.leilao_acompanhar
		})
		.success(function(res){
			if (res.s){
			 	checarNotificacao();
			}else{
				window.alert(res.msg);
			}
			jQuery('#checkNotificar').attr("disabled", false);
		});
	};

	var obterLeilao = function(leilao_numero){
		$http.get('/rest/leilao/'+leilao_numero)
		.success(function(res) {
			if (res.s) {
				$scope.leilao = res.r;
				checarNotificacao();
			}else{
				window.alert(res.msg);
			}
		});
	};

	Init();
};