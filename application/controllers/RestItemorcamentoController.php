<?php

class RestItemorcamentoController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
	}

    public function obterItensPorOrcamentoAction()
    {
     	$modelOrcamento = new Application_Model_DbTable_Itemorcamento();
     	$resuldado = $modelOrcamento->obterItensPorOrcamento($this->getRequest()->getParam('orcamento_numero'));   
    	
    	$this->getResponse()->setBody(Zend_Json::encode($resuldado));
    }

}







