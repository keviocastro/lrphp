<?php

class RestLeilaoController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        if ($this->getRequest()->getParam('action') == 'criar-alterar') {
            switch ($this->getRequest()->getMethod()) {
                case 'POST':
                $this->_forward('criar', $this->getRequest()->getParam('controller'), $this->getRequest()->getParam('module'), $this->getRequest()->getParams());
                break;
                case 'PUT':
                $this->_forward('alterar', $this->getRequest()->getParam('controller'), $this->getRequest()->getParam('module'), $this->getRequest()->getParams());
                break;
            }
        }
        
        parent::init();
    }

    public function criarAction()
    {
        $modelLeilao = new Application_Model_DbTable_Leilao();
        $resultado =  $modelLeilao->criar(Zend_Json::decode($this->getRequest()->getRawBody())['itens']);
        
        $this->getResponse()->setBody(Zend_Json::encode($resultado));
        
    }  

    public function alterarAction()
    {
        $modelLeilao = new Application_Model_DbTable_Leilao();
        $leilao = Zend_Json::decode($this->getRequest()->getRawBody());
        $resultado = $modelLeilao->alterar($leilao);
        
        $this->getResponse()->setBody(Zend_Json::encode($resultado));
    }


    public function obterAction()
    {   
        $modelLeilao = new Application_Model_DbTable_Leilao();
        $resultado =  $modelLeilao->obter($this->getRequest()->getParam('leilao_numero'));
        $this->getResponse()->setBody(Zend_Json::encode($resultado));
    }

    public function obterComItensAction()
    {
        $modelLeilao = new Application_Model_DbTable_Leilao();
        $resultado =  $modelLeilao->obterComItens($this->getRequest()->getParam('leilao_numero'));
        $this->getResponse()->setBody(Zend_Json::encode($resultado));
    }

    public function iniciarAction()
    {  
        $modelLeilao = new Application_Model_DbTable_Leilao();
        $leilao = Zend_Json::decode($this->getRequest()->getRawBody());
        $resultado =  $modelLeilao->iniciar($leilao);

        $this->getResponse()->setBody(Zend_Json::encode($resultado));
    }

    public function obterRankingAction()
    {
        $modelLeilao = new Application_Model_DbTable_Leilao();
        $resultado =  $modelLeilao->obterRanking($this->getRequest()->getParam('leilao_numero'));
        $this->getResponse()->setBody(Zend_Json::encode($resultado));
    }
}







