<?php

class RestOrcamentoController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
	}

    public function criarAction()
    {
        $modelOrcamento = new Application_Model_DbTable_Orcamento();
        $orcamento = Zend_Json::decode($this->getRequest()->getRawBody());
        $resultado =  $modelOrcamento->criar($orcamento);
        
        $this->getResponse()->setBody(Zend_Json::encode($resultado));
    }

}







