<?php

class Application_Model_DbTable_Orcamento extends Zend_Db_Table_Abstract {

    protected $_name = 'orcamento';

    public function criar($orcamento)
    {
        $resultado = new stdClass(); 
        $itens = $orcamento['itens'];   
        unset($orcamento['itens']);

        $dataAtual = Zend_Date::now();
        $orcamento['orcamento_data'] = $dataAtual->get('WWW');
        $orcamento['orcamento_valorentrega'] = str_replace(',', ".", $orcamento['orcamento_valorentrega']);
        
        if (empty($itens)) {
            $resultado->s = false;
            $resultado->msg = 'Erro ao criar o orçamento';
            $resultado->erro = 'Não foi definido o valor dos itens';
            return $resultado;
        }

        try {
            // Cria o leilão
            $orcamento_numero = $this->insert($orcamento);

            //Cria os itens do leilão
            $orcamento['orcamento_numero'] = $orcamento_numero;
            $modelItemOrcamento = new Application_Model_DbTable_Itemorcamento();
            foreach ($itens as $item) {
                $item['orcamento_numero'] = $orcamento_numero;
                $item['itemorcamento_data'] = $dataAtual;
                $item['itemorcamento_valortotal'] = str_replace(',', '.', $item['itemorcamento_valortotal']);

                try {
                    
                    $modelItemOrcamento->insert($item);
                } catch (Exception $e) {
                    $resultado->s = false;
                    $resultado->msg = 'Erro ao criar os itens do orçamento';
                    $resultado->erro = $e->getMessage();
                    return $resultado;
                }
            }

        } catch (Exception $e) {
            $resultado->s = false;
            $resultado->msg = 'Erro ao criar o orçamento';
            $resultado->erro = $e->getMessage();
            return $resultado;
        }

        $resultado->s = true;
        $resultado->r = $orcamento;
        $resultado->msg = 'O orçamento foi criado';
        return $resultado;
    }
}

