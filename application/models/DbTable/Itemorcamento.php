<?php

class Application_Model_DbTable_Itemorcamento extends Zend_Db_Table_Abstract {

    protected $_name = 'itemorcamento';
    
    function obterItensPorOrcamento($orcamento_numero) {
        $resultado = new stdClass();
        try {
            $select = $this->select()->setIntegrityCheck(false)
            ->from('itemorcamento', array('itemorcamento.*','item.*'))
            ->joinInner('item', 'itemorcamento.item_id = item.item_id')
            ->joinInner('orcamento', 'orcamento.orcamento_numero = itemorcamento.orcamento_numero')
            ->where("itemorcamento.orcamento_numero = ".$orcamento_numero);
            
            $rows = $this->fetchAll($select);   
            $resultado->r = $rows->toArray();
        } catch (Exception $e) {
            $resultado->s = false;
            $resultado->msg = "Erro ao obter itens do ormcamento ".$orcamento_numero;
            $resultado->erro = $e->getMessage();
            return $resultado;
        }

        $resultado->s = true;
        return $resultado;
    }

   
}

