<?php

class Application_Model_DbTable_Leilao extends Zend_Db_Table_Abstract {

    protected $_name = 'leilao';

    /**
     * 
     * @param type $leilao_numero
     * @param type $limit
     * @return array
     */
    public function obterRanking($leilao_numero) {
        $resultado = new stdClass();
        
        try {
            $select = $this->select()->setIntegrityCheck(false)
            ->from('item', array('sum(itemorcamento.itemorcamento_valortotal) AS totalorcamento',
                'itemorcamento.orcamento_numero', 'item.leilao_numero', 'orcamento.*', 'leilao.*'))
            ->joinInner('itemorcamento', 'itemorcamento.item_id = item.item_id')
            ->joinInner('orcamento', 'orcamento.orcamento_numero = itemorcamento.orcamento_numero')
            ->joinInner('leilao', 'leilao.leilao_numero = item.leilao_numero')
            ->where("item.leilao_numero = ".$leilao_numero)
            ->GROUP('itemorcamento.orcamento_numero')
            ->ORDER('totalorcamento ASC');

            $rows = $this->fetchAll($select);
        } catch (Exception $e) {
            $resultado->s = false;
            $resultado->msg = "Erro ao obter o ranking do leilão";
            $resultado->erro = $e->getMessage();
            return $resultado;
        }
        
        $resultado->s = true;
        $resultado->r = $rows->toArray();
        return $resultado;
    }
    
    public function iniciar(array $leilao) {
        $resultado = new stdClass();
        $data = new Zend_Date();
        $fimEstimado = new Zend_Date($data->get('WWW'));
        $leilao['leilao_fimestimado'] =  $fimEstimado->addHour($leilao['leilao_tempo'])->get('WWW');

        try {
            $this->update($leilao, "leilao_numero = ".$leilao['leilao_numero']);
            $itemModel = new Application_Model_DbTable_Item();
            $resultItens = $itemModel->obter($leilao['leilao_numero']);
            if($resultItens->s){
                $leilao['itens'] = $resultItens->r;
                try {
                    $html = new Zend_View();
                    $html->setScriptPath(APPLICATION_PATH . '/views/scripts/orcamento/email/');
                    $html->assign('leilao', $leilao);
                    $html->assign('hostname', 'http://lr');
                    $htmlEmail = $html->render('solicitacao.html');
                    if (!empty($leilao['leilao_emails'])) {
                        $arrayEmail = explode(',', $leilao['leilao_emails']);
                        $email = '';
                        foreach ($arrayEmail as $key => $value) {
                            $email = trim($value);
                            $validator = new Zend_Validate_EmailAddress();
                            if( $validator->isValid($email) ){
                                try {
                                    $mail = new Zend_Mail('utf-8');
                                    $mail->setBodyHtml($htmlEmail);
                                    $mail->addTo($email);
                                    $mail->setSubject('Solicitação de orçamento');
                                    $mail->send();
                                } catch (Exception $e) {
                                     $resultado->s = false;
                                     $resultado->msg = 'Erro ao enviar email de solicitação de orçamento: '.$email;
                                     $resultado->erro = $e->getMessage();
                                     return $resultado;
                                }
                            }else{
                                $resultado->s = false;
                                $resultado->msg = 'Email inálido. '.$email;
                                return $resultado;
                            }
                        }
                    }
                } catch (Exception $e) {
                    $resultado->s = false;
                    $resultado->msg = 'Erro ao renderizar a view orcamento/email/solicitacao.';
                    $resultado->erro = $e->getMessage();
                    return $resultado;
                }
            }else{
                return $resultItens;
            }
                
        } catch (Exception $exc) {
            $resultado->s = false;
            $resultado->msg = 'Erro ao iniciar o leilão';
            $resultado->erro = $exc->getMessage();
            return $resultado;
        }

        $resultado->s = true;
        $resultado->msg = 'O leilão foi iniciado';
        $resultado->r = $leilao;
        
        return $resultado;
    }

    public function criar(array $itens) {
        $data = new Zend_Date();
        $r = new stdClass();

        $leilao['leilao_inicio'] = $data->get('WWW');
        $leilao['leilao_fim'] = null;
        $leilao['leilao_fimestimado'] = null;

        try {
            // Cria o leilão
            $leilao_numero = $this->insert($leilao);
            $leilao['leilao_numero'] = $leilao_numero;

            // Cria os itens do leilão
            try {
                $itemModel = new Application_Model_DbTable_Item();
                foreach ($itens as $item) {
                    $item['leilao_numero'] = $leilao_numero;
                    $itemModel->insert($item);
                }
            } catch (Exception $e) {
               $r->s = false;
                $r->msg = 'Erro ao criar os itens do leilão';
                $r->erro = $exc->getMessage();
                return $r;
            }

            $r->s = true;
            $r->r = $leilao;
            $r->msg = 'O leilao foi criado';

        } catch (Exception $exc) {
            $r->s = false;
            $r->msg = 'Erro ao criar o leilão';
            $r->erro = $exc->getMessage();
            return $r;
        }

        return $r;
    }

    public function obter($leilao_numero)
    {
        $resultado = new stdClass();

        try {
            $row = $this->fetchRow('leilao_numero = '.$leilao_numero);
        } catch (Exception $e) {
            $resultado->s = false;
            $resultado->msg = "Erro ao obter o leilão";
            $resultado->erro = $err;
            return $resultado;
        }

        $resultado->s = true;
        $resultado->r = $row->toArray();
        return $resultado;
       
    }

    public function alterar($leilao)
    {
        $resultado = new stdClass();

        try {
            $this->update($leilao, 'leilao_numero = '.$leilao['leilao_numero']);
        } catch (Exception $e) {
            $resultado->s = false;
            $resultado->erro = $e->getMessage();
            $resultado->msg = 'Erro ao atualizar o leilão';
            return $resultado;
        }

        $resultado->s = true;
        $resultado->msg = 'O leilão foi atualizado';
        $resultado->r = $leilao;
        return $resultado;
    }

    public function obterComItens($leilao_numero)
    {
        $resultado = new stdClass();
        try {
            $select = $this->select()->from('leilao')->setIntegrityCheck(false)
            ->joinInner('item', 'leilao.leilao_numero = item.leilao_numero')
            ->where("leilao.leilao_numero = $leilao_numero");
            $rows = $this->fetchAll($select);
        } catch (Exception $e) {
            $resultado->s = false;
            $resultado->msg = "Erro ao obter o leilao com itens";
            $resultado->erro = $e->getMessage();
            return $resultado;
        }

        $resultado->s = true;
        $resultado->r = $rows->toArray();
        return $resultado;        
    }

}

